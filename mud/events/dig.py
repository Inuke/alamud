# -*- coding: utf-8 -*-
# Copyright (C) 2015 Seyyid Duran, IUT d'Orléans
#==============================================================================

from .event import Event2

class DigEvent(Event2):
    NAME = "dig"

    def perform(self):
        if not self.object.has_prop("diggable"):
            self.fail()
            return self.inform("dig.failed")
        self.inform("dig")
