# -*- coding: utf-8 -*-
# Copyright (C) 2015 Seyyid Duran, IUT d'Orléans
#==============================================================================

from .event import Event2

class CallEvent(Event2):
    NAME = "call"

    def perform(self):
        if not self.object.has_prop("callable"):
            self.fail()
            return self.inform("call.failed")
        self.inform("call")
