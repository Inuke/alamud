# -*- coding: utf-8 -*-
# Copyright (C) 2015 Seyyid Duran, IUT d'Orléans
#==============================================================================

from .effect import Effect2
from mud.events import CallEvent

class CallEffect(Effect2):
    EVENT = CallEvent
