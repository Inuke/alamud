# -*- coding: utf-8 -*-
# Copyright (C) 2015 Seyyid Duran, IUT d'Orléans
#==============================================================================

from .effect import Effect2
from mud.events import DigEvent

class DigEffect(Effect2):
    EVENT = DigEvent
