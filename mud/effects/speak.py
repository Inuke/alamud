# -*- coding: utf-8 -*-
# Copyright (C) 2015 Seyyid Duran, IUT d'Orléans
#==============================================================================

from .effect import Effect2
from mud.events import SpeakEvent

class SpeakEffect(Effect2):
    EVENT = SpeakEvent
