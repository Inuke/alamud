# -*- coding: utf-8 -*-
# Copyright (C) 2015 Seyyid Duran, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import SpeakEvent

class SpeakAction(Action2):
    EVENT = SpeakEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "speak"
