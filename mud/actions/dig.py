# -*- coding: utf-8 -*-
# Copyright (C) 2015 Seyyid Duran, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import DigEvent

class DigAction(Action2):
    EVENT = DigEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "dig"
