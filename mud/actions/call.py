# -*- coding: utf-8 -*-
# Copyright (C) 2015 Seyyid Duran, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import CallEvent

class CallAction(Action2):
    EVENT = CallEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "call"
